const express = require('express');
const router2 = express.Router();

// Import units of functions from userController module
const { 
		createCourse,
		getAllCourses,
		courseProfile,
		updateCourse,
		isOffered0,
		isOffered1,
		activeCourses,
		deleteCourse


} = require('./../controllers/courseControllers');

const {verify, decode, verifyAdmin} = require('./../auth');



//Create a route /create to create a new course, save the course in DB and return the document
	//Only admin can create a course
	//STATUS: OK! <3
	router2.post('/create', verifyAdmin, async (req, res) => {
		console.log(req.body)	//user object

		try{
			await createCourse(req.body).then(result => res.send(result))

		} catch(err){
			res.status(500).json(err)
		}
	})


//Create a route "/" to get all the courses, return all documents found
	//both admin and regular user can access this route
	// STATUS: OK <3
	router2.get('/', async (req, res) => {

		try{
			await getAllCourses().then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})

//Create a route "/isActive" to get all active courses, return all documents found
	//both admin and regular user can access this route
	// STATUS: weird @_@ but working
	router2.get('/isActive', verify, async (req, res) => {

		try{
			await activeCourses().then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})

//Create a route "/:courseId" to retrieve a specific course, save the course in DB and return the document
	//both admin and regular user can access this route
	//STATUS: OK <3
	router2.get('/:courseId', verify, async (req, res) => {
		// console.log(req.headers.authorization)
		const reqId = req.params.courseId

		// const courseId = (decode(req.headers.authorization).id)
		console.log(reqId)

		try{
			courseProfile(reqId).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	}) 

//Create a route "/:courseId/update" to update course info, return the updated document
	//Only admin can update a course
	// STATUS: OK!! <3
	router2.put('/:courseId/update', verifyAdmin, async (req, res) => {
		// console.log(req.params)
		const courseId = req.params.courseId
		const edittedCourse = req.body
		// const userId = decode(req.headers.authorization).id
		// console.log(userId)

		try{
			updateCourse(courseId, edittedCourse).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	}) 

//Create a route "/:courseId/archive" to update isActive to false, return updated document
	//Only admin can update a course
	//STATUS: OK

	router2.patch('/:id/archive', verifyAdmin, async (req, res) => {
		 // console.log('this is working')

		try{
			await isOffered0(req.params.courseId).then(result => res.send(result))
		} catch(err){
		 	res.status(500).json(err)
		}
		 
	}) 

//Create a route "/:courseId/archive" to update isActive to true, return updated document
	//Only admin can update a course
	//STATUS: OK

	router2.patch('/:id/unarchive', verifyAdmin, async (req, res) => {
		 // console.log('this is working')

		try{
			await isOffered1(req.params.courseId).then(result => res.send(result))
		} catch(err){
		 	res.status(500).json(err)
		}
		 
	}) 

//Create a route "/:id/delete-course" to delete a courses, return true if success
	//Only admin can delete a course


	router2.delete('/:id/delete', verifyAdmin, async (req, res) => {

		 
		 try{
			await deleteCourse(req.body).then(result => res.send(result))

		 }catch(err){
		 	res.status(500).json(err)
		 }
		 
	}) 





//Export the router module to be used in index.js file
module.exports = router2;
