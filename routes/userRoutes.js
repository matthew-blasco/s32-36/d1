const express = require('express');
const router1 = express.Router();

// Import units of functions from userController module
const {
	register,
	getAllUsers,
	checkEmail,
	login,
	profile,
	update,
	updatePassword,
	updateAuth1,
	updateAuth0,
	deleteDoc
	
} = require('./../controllers/userControllers');

const {verify, decode, verifyAdmin} = require('./../auth');

//GET ALL USERS
router1.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//REGISTER A USER 
router1.post('/register', async (req, res) => {
	// console.log(req.body)	//user object

	try{
		await register(req.body).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})


//CHECK IF EMAIL ALREADY EXISTS
router1.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})



//LOGIN THE USER
	// authentication
router1.post('/login', (req, res) => {
	// console.log(req.body)
	try{
		login(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//RETRIEVE USER INFORMATION
router1.get('/profile', verify, async (req, res) => {
	// console.log(req.headers.authorization)
	const userId = decode(req.headers.authorization).id
	// console.log(userId)

	try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
}) 


//Create a route to update user information, make sure the route is secured with token

router1.put('/user-password', verify, async (req, res) => {
	// console.log(req.headers.authorization)
	const userId = decode(req.headers.authorization).id
	// console.log(userId)

	try{
		update(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
}) 


//Create a route to update user's password, make sure the  route is secured with token

router1.patch('/user-password', verify, async (req, res) => {
	// console.log(decode(req.headers.authorization).id)
	// console.log(req.body)

	 const userId = decode(req.headers.authorization).id

	try{
		await updatePassword(userId, req.body.password).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
	
}) 

//Create a route /isAdmin to update user's isAdmin status to true, make sure the  route is secured with token
	//Only admin can update user's isAdmin status

router1.patch('/isAdmin', verify, async (req, res) => {
	 // console.log(req.headers)
	 // console.log(req.body.email)
	 const email = req.body.email
	 const adminStatus = req.body.isAdmin

	 try{
	 	await updateAuth(email, adminStatus).then(result => res.send(result))

	 }catch(err){
	 	res.status(500).json(err)
	 }
	 
}) 

//Create a route /isUser to update user's isAdmin status to false, make sure the  route is secured with token
	//Only admin can update user's isAdmin status


router1.patch('/isUser', verify, async (req, res) => {
	 // console.log(req.headers)
	 // console.log(req.body.email)
	 const adminStatus = (decode(req.headers.authorization).isAdmin)
	 const email = req.body.email
	 
	 try{
	 	if(adminStatus == true){
			await updateAuth0(email).then(result => res.send(result))
	 } else {
	 	res.send(`You are not authorized!`)
	 }

	 }catch(err){
	 	res.status(500).json(err)
	 }
	 
}) 




//Create a route to /delete a user from the DB, make sure the route is secured
	// Stretch goal, only admin can delete a user

router1.delete('/delete', verify, async (req, res) => {
	 // console.log(req.headers)
	 // console.log(req.body.email)
	 const adminStatus = (decode(req.headers.authorization).isAdmin)

	 
	 try{
	 	if(adminStatus == true){
			await deleteDoc(req.body).then(result => res.send(result))
	 } else {
	 	res.send(`You are not authorized!`)
	 }

	 }catch(err){
	 	res.status(500).json(err)
	 }
	 
}) 




//Export the router module to be used in index.js file
module.exports = router1;
