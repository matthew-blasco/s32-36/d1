const CryptoJS = require("crypto-js");
// const bcrypt = require('bcrypt');
const User = require('./../models/User');
const Course = require('./../models/Course');

//import createToken function from auth module
const {createToken} = require('./../auth');


//REGISTER A USER
module.exports.register = async (reqBody) => {
	// console.log(reqBody)
	const {firstName, lastName, email, password} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName, 
		email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
		// password: bcrypt.hashSync(password, 10)
	})

	return await  newUser.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


//GET ALL USERS
module.exports.getAllUsers = async () => {

	return await User.find().then(result => result)
}


//CHECK IF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) => {
	const {email} = reqBody

	return await User.findOne({email: email}).then((result, err) =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			} else {
				return err
			}
		}
	})
}



//LOGIN A USER
module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				//check if pw is correct, decrypt to compare pw input from the user from pw stored in db
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				console.log(reqBody.password == decryptedPw) //true
				

				if(reqBody.password == decryptedPw){
					//create a token for the user
					return { token: createToken(result) }
				} else {
					return {auth: `Auth Failed!`}
				}

			} else {
				return err
			}
		}
	})
}



//RETRIEVE USER INFORMATION
module.exports.profile = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `user does not exist`}
			} else {
				return err
			}
		}
	})
}


// UPDATE USER INFO
module.exports.update = async (userId, reqBody) => {
	const userData = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
	}

	return await User.findByIdAndUpdate(userId, {$set: userData}, {new:true}).then((result, err) => {
		// console.log(result)
		if(result){
			result.password = "***"
			return result
		} else{
			return err
		}
	})
}


//UPDATE PASSWORD INFORMATION

module.exports.updatePassword = async (id, password) =>{
	let updatedPassword ={
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
	}
	return await User.findByIdAndUpdate(id, {$set: updatedPassword}, {new:true}).then((result,err)=>{
		if(result){
			result.password ="***"
			return result
		} else {
			return err
		}
	})
}


//UPDATE ADMIN STATUS = TRUE

module.exports.updateAuth1 = async (email) =>{
	let newStatus1 ={
		isAdmin: true
	}
	return await User.findOneAndUpdate(email, {$set: newStatus1}, {new:true}).then((result,err)=>{
		if(result){
			result.isAdmin ="Has now been changed to true"
			return result
		} else {
			return err
		}
	})
}


//UPDATE ADMIN STATUS = FALSE

module.exports.updateAuth0 = async (email) =>{
	let newStatus0 = {isAdmin: false}
	return await User.findOneAndUpdate(email, {$set: newStatus0}, {new:true}).then((result,err)=>{
		if(result){
			result.isAdmin ="Has now been changed to false"
			return result
		} else {
			return err
		}
	})
}

//DELETE DOC 

module.exports.deleteDoc = async (delThis) =>{
	const {email} = delThis

	return await User.findOneAndDelete({email: email}).then((result,err)=>{
		if(result){
			
			return true
		} else {
			return err
		}
	})
}


//MA'AM JOY HAS OTHER SOLUTION: