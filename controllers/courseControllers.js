const CryptoJS = require("crypto-js");
// const bcrypt = require('bcrypt');
const User = require('./../models/User');
const Course = require('./../models/Course');

//import createToken function from auth module
	const {createToken} = require('./../auth');



//CREATE A COURSE
	module.exports.createCourse = async (reqBody) => {
		// console.log(reqBody)
		const {courseName, description, price} = reqBody

		const newCourse = new Course({
			courseName: courseName,
			description: description, 
			price: price,
		})

		return await  newCourse.save().then(result => {
			if(result){
				return true
			} else {
				if(result == null){
					return false
				}
			}
		})
	}

//GET ALL COURSES

	module.exports.getAllCourses = async () => {

		return await Course.find().then(result => result)
	}


//RETRIEVE SPECIFIC COURSE INFORMATION

	module.exports.courseProfile = async (id) => {
		return await Course.findById(id).then((result, err) => {
			if(result){
				return result
			}else{
				if(result == null){
					return {message: `course does not exist`}
				} else {
					return err
				}
			}
		})
	}


//UPDATE SPECIFIC COURSE INFORMATION

	module.exports.updateCourse = async (courseId, reqBody) => {
		const courseData = {

			courseName: reqBody.courseName,
		    description: reqBody.description,     
		    price: reqBody.price
		
		}
		return await Course.findByIdAndUpdate(courseId, {$set: courseData}, {new:true}).then((result, err) => {
			// console.log(result)
			if(result){
				
				return result
			} else{
				return err
			}
		})
	}


//UPDATE isOffered = false

	module.exports.isOffered0 = async (id) =>{
		let isOfferedn0 ={isOffered: false}
		return await Course.findOneAndUpdate(id, {$set: isOfferedn0}, {new:true}).then((result,err)=>{
			if(result){
				return (result + ' [io#0] This document has been modified by an Admin')
			} else {
				return err
			}
		})
	}

//UPDATE isOffered = true

	module.exports.isOffered1 = async (id) =>{
		let isOfferedn1 ={isOffered: true}
		return await Course.findOneAndUpdate(id, {$set: isOfferedn1}, {new:true}).then((result,err)=>{
			if(result){
				return (result + ' [io#1] This document has been modified by an Admin')
			} else {
				return err
			}
		})
	}


//GET ALL documents isOffered = true

	module.exports.activeCourses = async () => {
		// const query = Character.find({ name: 'Jean-Luc Picard' });
		// query.getFilter();

		return await Course.find({isOffered:true}).then(result => result)
	}


//DELETE DOC 

	module.exports.deleteCourse = async (courseDel) =>{
		const {courseName} = courseDel

		return await Course.findOneAndDelete({courseName: courseName}).then((result,err)=>{
			if(result){
				
				return true
			} else {
				return err
			}
		})
	}

